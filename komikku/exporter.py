# Copyright (C) 2019-2021 Valéry Febvre
# SPDX-License-Identifier: GPL-3.0-only or GPL-3.0-or-later
# Author: Valéry Febvre <vfebvre@easter-eggs.com>

from gettext import gettext as _
from gettext import ngettext as n_

import PIL
import os
import threading

from komikku.models import Chapter

from gi.repository import GObject
from gi.repository import Gtk

class Exporter(GObject.GObject):
    """
    Mangas exporter to PDF
    """

    queue = []
    running = False

    def __init__(self, window):
        GObject.GObject.__init__(self)

        self.window = window


    def add(self, mangas):
        if not isinstance(mangas, list):
            mangas = [mangas, ]
        mangas.reverse()

        chooser = Gtk.FileChooserDialog(title=_("Select Export Directory"),
                                        action=Gtk.FileChooserAction.SELECT_FOLDER,
                                        buttons=(Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL,
                                                 Gtk.STOCK_OPEN,
                                                 Gtk.ResponseType.OK))
        chooser.set_default_response(Gtk.ResponseType.OK)

        if chooser.run() == Gtk.ResponseType.OK:
            filename = chooser.get_filename()
            chooser.destroy()
        else:
            chooser.destroy()
            return

        for manga in mangas:
            if manga.id not in self.queue:
                self.queue.append([manga.id, filename])


    def start(self):
        if not self.running:
            self.running = True
            self.iterate()


    def iterate(self):
        if len(self.queue) == 0:
            self.running = False
        else:
            chapter_id, save_uri = self.queue[0]
            export_thread = threading.Thread(target=self.call_export, args=(chapter_id, save_uri))
            export_thread.start()


    def call_export(self, chapter_id, save_uri):
        self.export(chapter_id, save_uri)
        self.queue = self.queue[1:]
        self.iterate()


    def export(self, chapter_id, save_uri):
        chapter = Chapter.get(chapter_id)

        i=0
        pages_list = []
        finished = False
        while not finished and i < len(chapter.pages):
            page_path = chapter.get_page(i)
            if page_path is None:
                finished = True
            else:
                pages_list.append(page_path)
                i+=1

        images = []
        for page in pages_list:
            images.append(PIL.Image.open(page).convert("RGB"))

        export_path = save_uri + '/' + str(chapter_id) + '.pdf'
        images[0].save(export_path, save_all=True, append_images=images[1:])

        self.window.show_notification(_("Manga exported to ") + export_path)

